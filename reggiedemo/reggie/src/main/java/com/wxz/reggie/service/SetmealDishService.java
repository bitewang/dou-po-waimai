package com.wxz.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxz.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
