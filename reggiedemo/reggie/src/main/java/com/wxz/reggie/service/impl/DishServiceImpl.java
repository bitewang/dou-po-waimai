package com.wxz.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxz.reggie.dto.DishDto;
import com.wxz.reggie.entity.Dish;
import com.wxz.reggie.entity.DishFlavor;
import com.wxz.reggie.mapper.DishMapper;
import com.wxz.reggie.service.DishFlavorService;
import com.wxz.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    @Resource
    private DishMapper dishMapper;
    /**
     * 新增菜品 同时保存对应口味的数据
     * @param dishDto
     */
    @Override
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        // 保存菜品基本信息到菜表
        this.save(dishDto);

        // 菜品id
        Long dishId = dishDto.getId();
        // 保存菜品口味数据到菜品口味表dish_flavor
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors.stream().map((item) -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());


        dishFlavorService.saveBatch(flavors);



    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */

    @Override
    public DishDto getByIdWithFlavor(Long id) {
        Dish dish = this.getById(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish, dishDto);

        // 查询当前菜品对应的口味信息，从dish_flavor表查询
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        dishDto.setFlavors(flavors);
        return dishDto;

    }

    @Override
    public void deleteById(Long ids) {
        dishMapper.deleteById(ids);
    }

    @Override
    public void updateWithFlavor(DishDto dishDto) {
            // 更新dish表的基本信息系
            this.updateById(dishDto);

            // 清理当前菜品对应口味数据--dish_flavor表的delete操作
            LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());

            dishFlavorService.remove(queryWrapper);

            // 添加当前提交的口味数据--dish_flavor表的insert操作

            List<DishFlavor> flavors = dishDto.getFlavors();

            flavors.stream().map((item) -> {
                item.setDishId(dishDto.getId());
                return item;
            }).collect(Collectors.toList());

            dishFlavorService.saveBatch(flavors);

            // 添加当前提交过来的口味数据--dish_flavor表的insert操作
    }
}
