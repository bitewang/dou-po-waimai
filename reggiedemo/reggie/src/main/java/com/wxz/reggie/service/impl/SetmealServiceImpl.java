package com.wxz.reggie.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxz.reggie.common.CustomException;
import com.wxz.reggie.dto.SetmealDto;
import com.wxz.reggie.entity.Setmeal;
import com.wxz.reggie.entity.SetmealDish;
import com.wxz.reggie.mapper.SetmealMapper;
import com.wxz.reggie.service.SetmealDishService;
import com.wxz.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;
    /**
     * 新增套餐，同时保存套餐和菜品的关联关系
     * @param setmealDto
     */
    @Override
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        // 保存套餐的基本信息，操作Setmeal，执行insert操作
        this.save(setmealDto);

        // 套餐id拿出来
        Long setmealId = setmealDto.getId();

        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealId);
            return item;
        }).collect(Collectors.toList());

        // 保存套餐和菜品的关联信息，操作setmeal_dish，执行insert操作
            setmealDishService.saveBatch(setmealDishes);

    }

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联关系
     * @param ids
     */
    @Override
    public void removeWithDish(List<Long> ids) {
        // 查询状态，确认是否可用删除
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        // sql -> select count(*) from setmeal where id in (1,2,3) and status = 1;
        queryWrapper.in(Setmeal::getId, ids).eq(Setmeal::getStatus,1);

        int count = this.count(queryWrapper); // 如果查到的count大于0表示当前还有在售的套餐

        // 如果不能删除，抛出一个业务异常
        if (count > 0) {
            throw new CustomException("套餐正在售卖中，不能删除");
        }

        // 如果可以删除，先删除套餐表中的数据 -> setmeal
        this.removeByIds(ids);

        // sql -> delete from setmeal_dish where setmeal_id in (1,2,3)
        // 删除关系表中的数据 -> setmealdish
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId, ids);
        setmealDishService.remove(lambdaQueryWrapper);



    }
}
