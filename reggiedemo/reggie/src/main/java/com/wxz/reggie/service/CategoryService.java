package com.wxz.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxz.reggie.entity.Category;

public interface CategoryService extends IService<Category> {
    void remove(Long id);
}
