package com.wxz.reggie.dto;

import com.wxz.reggie.entity.Setmeal;
import com.wxz.reggie.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
