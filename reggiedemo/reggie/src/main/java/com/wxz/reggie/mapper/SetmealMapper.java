package com.wxz.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxz.reggie.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
