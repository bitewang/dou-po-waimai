package com.wxz.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxz.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}
