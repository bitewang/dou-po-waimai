package com.wxz.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxz.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
